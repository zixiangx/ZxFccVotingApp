﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Microsoft.Extensions.Options;
using ZxFccVotingApp.Models;
using Newtonsoft.Json;
using ZxFccVotingApp.Models.Utils;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ZxFccVotingApp.Controllers
{

    public class LoginController : Controller
    {
        private readonly FacebookAuthenticationOptions _fbAuthOptions;
        private readonly FacebookSessionOptions _fbSessionOptions;

        public LoginController(IOptions<FacebookAuthenticationOptions> fbAuthOptions, IOptions<FacebookSessionOptions> fbSessionOptions)
        {
            _fbAuthOptions = fbAuthOptions.Value;
            _fbSessionOptions = fbSessionOptions.Value;
        }
        
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Fb(string code)
        {
            // Construct Exchange Uri
            string uri = ConstructUri(code);

            // GET request to Exchange Uri
            var client = new HttpClient();
            string response = await client.GetStringAsync(uri);

            // Deserialize response from GET
            var responseModel = JsonConvert.DeserializeObject<FacebookResponseModel>(response);

            // Store token into session
            if (responseModel.Access_Token != null)
            {
                SessionExtensions.Set(HttpContext.Session, _fbSessionOptions.SessionKey, responseModel.Access_Token);
            }

            return Content(responseModel.Access_Token);
        }

        private string ConstructUri(string code)
        {
            string appid = _fbAuthOptions.AppId;
            string redirectUri = _fbAuthOptions.RedirectUri;
            string clientSecret = _fbAuthOptions.ClientSecret;
            string accessCode = code;

            return String.Format(_fbAuthOptions.ExchangeUri, appid, redirectUri, clientSecret, code);
        }

        //private async Task<string> GetAccessToken(string res)
        //{
        //    HttpClient client = new HttpClient();

        //    FacebookResponseModel responseModel = JsonConvert.DeserializeObject<FacebookResponseModel>(res);

        //    string uriString = String.Format(_options.VerificationUri, responseModel.Access_Token);

        //    Task<string> response = client.GetStringAsync(uriString);

        //    return await response;
        //}
    }
}
