﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccVotingApp.Models
{
    public class FacebookSessionOptions
    {
        public string SessionKey { get; set; }
        public int TimeoutInSeconds { get; set; }
    }
}
