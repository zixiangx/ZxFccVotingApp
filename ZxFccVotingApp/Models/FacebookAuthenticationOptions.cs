﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZxFccVotingApp.Models
{
    public class FacebookAuthenticationOptions
    {
        public string AppId { get; set; }
        public string RedirectUri { get; set; }
        public string ClientSecret { get; set; }
        public string ExchangeUri { get; set; }
        public string VerificationUri { get; set; }
    }
}
